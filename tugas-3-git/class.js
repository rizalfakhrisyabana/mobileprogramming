// No.1

class Animal {
    // Code class di sini
    constructor(name) {
        this.animalName = name;
        this.animalLegs = 4;
        this.animalCold_blooded = false;
    }
    get name() {
        return this.animalName;
    }
    get legs() {
        return this.animalLegs;
    }
    get cold_blooded() {
        return this.animalCold_blooded;
    }
    set name(name) {
        this.animalName = name;
    }
    set legs(legs) {
        this.animalLegs = legs;
    }
    set cold_blooded(blood) {
        this.animalBlood = blood;
    }
}


var sheep = new Animal("shaun");
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log('\n=====\n');

class Ape extends Animal {
    constructor(name) {
        super(name)
        this.animalName = name;
        this.animalLegs = 2;
    }
    yell() {
        console.log("Auooo");
        return
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name)
        this.animalName = name;
    }
    jump() {
        console.log("hop hop");
        return
    }
}

// Code class Ape dan class Frog di sini
var sungokong = new Ape("kera sakti");
sungokong.yell(); // "Auooo"
console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.cold_blooded)

console.log('\n=====\n');

var kodok = new Frog("buduk");
kodok.jump(); // "hop hop"
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded)

// No.2
console.log('\n');

// function di atas diubah menjadi struktur class seperti berikut:
class Clock {
    // Code di sini
    constructor({
        template
    }) {
        this.template = template
    }
    render() {
        let date = new Date();
        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
        let output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
        console.log(output);
    }
    stop() {
        clearInterval(timer);
    };
    start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);

    };
}

var clock = new Clock({
    template: 'h:m:s'
});
clock.start();