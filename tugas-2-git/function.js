function Teriak(){
    return("Halo Humanika!");
  }
  console.log(Teriak()) // "Halo Humanika!"

console.log("\n");

function kalikan(num1_param, num2_param) {
    return num1_param * num2_param;
  }
  
  var num1 = 12;
  var num2 = 14;
  
  var hasilkali = kalikan(num1,num2);
  console.log(hasilkali); // Menampilkan angka 48

  console.log("\n");

  function introduce(name_param, age_param, address_param, hobby_param) {
  return("Nama saya " + name_param + ", umur saya " + age_param + ", alamat saya di " + address_param + ", dan saya punya hobby yaitu " + hobby_param);
}

var name = "Rizal";
var age = 22;
var address = "Jl.Kapten Halim Simpang";
var hobby = "Olahraga";

var perkenalan = introduce(name,age,address,hobby);
console.log(perkenalan); // Menampilkan "Nama saya Rizal, umur saya 22 tahun, alamat saya di Jl.Kapten Halim Simpang, dan saya punya hobby yaitu Olahraga!"
